package com.ecoinomi.trustwallet.UI.Models;

public class USDValue {
    private Double USD;

    public Double getUSD() {
        return USD;
    }

    public void setUSD(Double USD) {
        this.USD = USD;
    }
}
