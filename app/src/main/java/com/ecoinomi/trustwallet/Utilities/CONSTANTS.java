package com.ecoinomi.trustwallet.Utilities;

public class CONSTANTS {


    public static String DeviantMulti = "DEVIANTMULTI "; //Authentication @API Services
    public static String usrnm = "USERNAME";  //USerName --- @LoginActivity
    public static String email = "EMAIL";  //Email --- @LoginActivity
    public static String pswd = "PASSWORD"; // Password --- @LoginActivity
    public static String wallet = "WALLET"; //Wallet --- @ SetUpWalletActivity
    public static String token = "TOKEN";     //Account token for Authorization @API Services
    public static String TAG = "!!--TrustWallet--!!";  // Console/LogCat
    public static String walletName = "WALLETNAME";   // User Wallet Name @ SetUp Wallet Activity
    public static String hideBal = "HIDEBALANCE";  //  Hiding Balance @AppSettingsActivity
    public static String screenshot = "SCREENSHOT";  //  Disable Screenshots @AppSettingsActivity
    public static String lightmode = "LIGHTMODE";  //  lIGHT MODE @AppSettingsActivity
    public static String selectedAccountWallet = "SELECTEDACCOUNTWALLET"; // Selected Account Wallet for storing data@ Adapters

    public static String sender = "SENDER"; // Sender @ Adapters
    public static String receiver = "RECEIVER"; // Sender @ Adapters
    public static String total_avail_bal = "TOTALAVAILBAL";
    public static String usdValue = "USDVALUE";
    public static String first_time = "FIRSTTIME";
    public static String allTags = "allTags"; //All Tags (SEEDS)
    public static String selectedTags = "selectedTags"; // Selected Tags (SEEDS)
    public static String seed = "SEED"; // Accound Seed (Recovery Phrase)

}
