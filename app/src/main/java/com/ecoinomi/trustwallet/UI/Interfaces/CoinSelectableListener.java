package com.ecoinomi.trustwallet.UI.Interfaces;

import com.ecoinomi.trustwallet.UI.Models.AllCoins;

import java.util.ArrayList;

public interface CoinSelectableListener {
    public void CoinSelected(ArrayList<AllCoins> allCoinsList);
}
